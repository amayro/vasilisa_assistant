import datetime
import os
import re
import time
import webbrowser

import dateparser
import requests
import speech_recognition as sr
from bs4 import BeautifulSoup
from gtts import gTTS
from pygame import mixer
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

try:
    from config import vk_login, vk_password
except:
    vk_login = ''
    vk_password = ''


class Vasilisa:

    def __init__(self):
        """Инициализация ассистента, определение окружающего шума"""
        self.rec = sr.Recognizer()
        self.mic = sr.Microphone(chunk_size=1024, sample_rate=48000)
        self.assistant_names = ['Василиса', 'Вася', 'Вась']

        print("\nInitializing...")
        with self.mic as source:
            self.rec.adjust_for_ambient_noise(source)
            self.rec.energy_threshold = 200
            print("\tThreshold: " + str(self.rec.energy_threshold))

        print("Могу выполнить несколько команд, начни фразу с моего имени и скажи что хочешь",
              "\nМожешь обращаться ко мне:", ', '.join(self.assistant_names))

        print("Возможные команды:"
              "\n\t... скажи *фраза* - повторю любую фразу"
              "\n\t... включи музыку - включу музыку c сайта Вконтакте"
              "\n\t... яндекс новости - расскажу о главных новостях с яндекс сайта"
              "\n\t... включи таймер *время* - включу таймер на указанное время"
              "\n\t... сколько время - скажу сколько время"
              "\n\t... открой яндекс - открою сайт яндекс\n"
              )

    @staticmethod
    def _play_audio(mp3_name):
        mixer.init()
        mixer.music.load(mp3_name)
        mixer.music.play()
        while mixer.music.get_busy():
            time.sleep(0.1)

    def speak(self, text=None, mp3_name=None):
        dir_audio = 'audio'

        if mp3_name:
            mp3_name = os.path.join(dir_audio, mp3_name) + '.mp3'
            if os.path.exists(mp3_name):
                return self._play_audio(mp3_name)
            else:
                return print(f'Ошибка. Такого mp3 нет: {mp3_name}')

        #  чтобы избежать коллизии при удалении mp3 -> mp3_nameold (имя предыдущего файла)
        mp3_nameold = '_'
        mp3_name = "_1.mp3"
        mp3_nameold = os.path.join(dir_audio, mp3_nameold)
        mp3_name = os.path.join(dir_audio, mp3_name)

        # Текст разбиваем по предложениям, чтобы избежать коллизий
        split_regex = re.compile(r'[.|!|?|…]')
        sentences = filter(lambda t: t, [t.strip() for t in split_regex.split(text)])

        for x in sentences:
            print(x)
            tts = gTTS(text=x, lang='ru')
            tts.save(mp3_name)

            self._play_audio(mp3_name)

            if os.path.exists(mp3_nameold) and not mp3_nameold.endswith('1.mp3'):
                os.remove(mp3_nameold)
            mp3_nameold = mp3_name
            now_time = datetime.datetime.now()
            mp3_name = now_time.strftime("%d%m%Y%I%M%S") + ".mp3"

        # чтобы удалить файл без коллизий, откроем сторонний mp3
        mixer.music.load(os.path.join(dir_audio, 'hello.mp3'))
        if os.path.exists(mp3_nameold):
            os.remove(mp3_nameold)

    def vk_music_on(self):
        if not vk_login or not vk_password:
            return self.speak(mp3_name='input_data_vk')

        self.speak(mp3_name='music_on')
        self._driver = webdriver.Chrome()
        url = 'https://vk.com'
        self._driver.get(url)

        self._web_wait('#index_email')
        field_login = self._driver.find_element_by_css_selector('#index_email')
        field_login.send_keys(vk_login)
        field_pass = self._driver.find_element_by_css_selector('#index_pass')
        field_pass.send_keys(vk_password)
        time.sleep(1)
        field_pass.send_keys(Keys.ENTER)

        self._web_wait('#l_aud')
        field_audio = self._driver.find_element_by_css_selector('#l_aud')
        field_audio.click()

        self._web_wait('.audio_page__shuffle_all_button')
        field_audio_shuffle = self._driver.find_element_by_css_selector('.audio_page__shuffle_all_button')
        field_audio_shuffle.click()

    def _web_wait(self, selector, timeout=3):
        WebDriverWait(self._driver, timeout).until(EC.presence_of_element_located((By.CSS_SELECTOR, selector)))

    def set_timer(self, text):
        # Отсекаем все лишнее в тексте
        time_str = text.split(' на ')[1]
        if len(time_str.split(' ')) > 2:
            time_str = ' '.join(time_str.split(' ')[0:1])

        self.speak(f'Включаю таймер на {time_str}')
        time = str((datetime.datetime.now() - dateparser.parse(time_str)).total_seconds()).split('.')[0]
        url = 'https://onlinetimer.ru/#!/set-time?seconds=' + time
        webbrowser.open(url)

    def yandex_news(self):
        self.speak(mp3_name='read_news_yandex')
        response = requests.get('https://yandex.ru')
        soup = BeautifulSoup(response.content, 'html.parser')
        pars_block = soup.select_one('#tabnews_newsc')

        pars_news = pars_block.findAll('li')
        news_arr = [i.text for i in pars_news]
        news_for_read = '. '.join(news_arr[:5])
        self.speak(news_for_read)

    def _callback(self, recognizer, audio):
        try:
            result = self.rec.recognize_google(audio, language="ru_RU")
            result = result.lower()
            self._action_definition(result)

        except sr.UnknownValueError:
            print("Google Speech Recognition could not understand audio")
        except sr.RequestError as e:
            print("Could not request results from Google Speech Recognition service; {0}".format(e))

    def _action_definition(self, text):
        print('Распознанный текст:', text)

        split_text = text.split(" ")
        first_word = split_text[0]

        if first_word.title() in self.assistant_names:
            if len(split_text) == 1:
                return self.speak(mp3_name='listen')

            if split_text[1] == 'скажи':
                self.speak(' '.join(split_text[2:]))

            elif 'открой яндекс' in text:
                self.speak(mp3_name='open')
                url = 'https://yandex.ru'
                webbrowser.open(url)

            elif 'включи музыку' in text:
                self.vk_music_on()

            elif 'включи таймер' in text:
                self.set_timer(text)

            elif 'сколько время' in text:
                self.speak(datetime.datetime.now().strftime("%H:%M"))

            elif 'яндекс новости' in text:
                self.yandex_news()

            else:
                self.speak(mp3_name='dont_understand')

    def start(self):
        self.rec.listen_in_background(self.mic, self._callback)
        self.speak(mp3_name='hello')

        while True:
            time.sleep(0.1)


assistant = Vasilisa()
assistant.start()
